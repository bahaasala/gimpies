﻿namespace Gimpies
{
    partial class NieuwProduct
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NieuwProduct));
            this.panel1 = new System.Windows.Forms.Panel();
            this.ExitIcon = new System.Windows.Forms.PictureBox();
            this.MinimizeIcon = new System.Windows.Forms.PictureBox();
            this.label_NieuwProduct = new System.Windows.Forms.Label();
            this.textbox_Merk = new System.Windows.Forms.TextBox();
            this.textbox_Type = new System.Windows.Forms.TextBox();
            this.textbox_Maat = new System.Windows.Forms.TextBox();
            this.textbox_Aantal = new System.Windows.Forms.TextBox();
            this.textbox_Prijs = new System.Windows.Forms.TextBox();
            this.label_Merk = new System.Windows.Forms.Label();
            this.label_Type = new System.Windows.Forms.Label();
            this.label_Maat = new System.Windows.Forms.Label();
            this.label_Aantal = new System.Windows.Forms.Label();
            this.label_Prijs = new System.Windows.Forms.Label();
            this.button_Toevoegen = new System.Windows.Forms.Button();
            this.label_text = new System.Windows.Forms.Label();
            this.BackIcon = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ExitIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinimizeIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BackIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.ExitIcon);
            this.panel1.Controls.Add(this.MinimizeIcon);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(553, 94);
            this.panel1.TabIndex = 10;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseUp);
            // 
            // ExitIcon
            // 
            this.ExitIcon.BackColor = System.Drawing.Color.Transparent;
            this.ExitIcon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ExitIcon.Image = ((System.Drawing.Image)(resources.GetObject("ExitIcon.Image")));
            this.ExitIcon.Location = new System.Drawing.Point(489, 15);
            this.ExitIcon.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ExitIcon.Name = "ExitIcon";
            this.ExitIcon.Size = new System.Drawing.Size(48, 48);
            this.ExitIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ExitIcon.TabIndex = 7;
            this.ExitIcon.TabStop = false;
            this.ExitIcon.Click += new System.EventHandler(this.ExitIcon_Click);
            // 
            // MinimizeIcon
            // 
            this.MinimizeIcon.BackColor = System.Drawing.Color.Transparent;
            this.MinimizeIcon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MinimizeIcon.Image = ((System.Drawing.Image)(resources.GetObject("MinimizeIcon.Image")));
            this.MinimizeIcon.Location = new System.Drawing.Point(437, 18);
            this.MinimizeIcon.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MinimizeIcon.Name = "MinimizeIcon";
            this.MinimizeIcon.Size = new System.Drawing.Size(44, 44);
            this.MinimizeIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.MinimizeIcon.TabIndex = 8;
            this.MinimizeIcon.TabStop = false;
            this.MinimizeIcon.Click += new System.EventHandler(this.MinimizeIcon_Click);
            // 
            // label_NieuwProduct
            // 
            this.label_NieuwProduct.AutoSize = true;
            this.label_NieuwProduct.BackColor = System.Drawing.Color.Transparent;
            this.label_NieuwProduct.Font = new System.Drawing.Font("Raleway", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_NieuwProduct.Location = new System.Drawing.Point(155, 111);
            this.label_NieuwProduct.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_NieuwProduct.Name = "label_NieuwProduct";
            this.label_NieuwProduct.Size = new System.Drawing.Size(220, 35);
            this.label_NieuwProduct.TabIndex = 11;
            this.label_NieuwProduct.Text = "Nieuw Product";
            this.label_NieuwProduct.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textbox_Merk
            // 
            this.textbox_Merk.Font = new System.Drawing.Font("Raleway", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textbox_Merk.Location = new System.Drawing.Point(89, 202);
            this.textbox_Merk.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textbox_Merk.Name = "textbox_Merk";
            this.textbox_Merk.Size = new System.Drawing.Size(132, 27);
            this.textbox_Merk.TabIndex = 12;
            // 
            // textbox_Type
            // 
            this.textbox_Type.Font = new System.Drawing.Font("Raleway", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textbox_Type.Location = new System.Drawing.Point(331, 202);
            this.textbox_Type.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textbox_Type.Name = "textbox_Type";
            this.textbox_Type.Size = new System.Drawing.Size(132, 27);
            this.textbox_Type.TabIndex = 13;
            // 
            // textbox_Maat
            // 
            this.textbox_Maat.Font = new System.Drawing.Font("Raleway", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textbox_Maat.Location = new System.Drawing.Point(89, 261);
            this.textbox_Maat.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textbox_Maat.Name = "textbox_Maat";
            this.textbox_Maat.Size = new System.Drawing.Size(132, 27);
            this.textbox_Maat.TabIndex = 14;
            // 
            // textbox_Aantal
            // 
            this.textbox_Aantal.Font = new System.Drawing.Font("Raleway", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textbox_Aantal.Location = new System.Drawing.Point(331, 261);
            this.textbox_Aantal.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textbox_Aantal.Name = "textbox_Aantal";
            this.textbox_Aantal.Size = new System.Drawing.Size(132, 27);
            this.textbox_Aantal.TabIndex = 15;
            // 
            // textbox_Prijs
            // 
            this.textbox_Prijs.Font = new System.Drawing.Font("Raleway", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textbox_Prijs.Location = new System.Drawing.Point(213, 318);
            this.textbox_Prijs.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textbox_Prijs.Name = "textbox_Prijs";
            this.textbox_Prijs.Size = new System.Drawing.Size(132, 27);
            this.textbox_Prijs.TabIndex = 16;
            // 
            // label_Merk
            // 
            this.label_Merk.AutoSize = true;
            this.label_Merk.BackColor = System.Drawing.Color.Transparent;
            this.label_Merk.Font = new System.Drawing.Font("Raleway", 8.999999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Merk.Location = new System.Drawing.Point(36, 208);
            this.label_Merk.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_Merk.Name = "label_Merk";
            this.label_Merk.Size = new System.Drawing.Size(44, 18);
            this.label_Merk.TabIndex = 17;
            this.label_Merk.Text = "Merk";
            // 
            // label_Type
            // 
            this.label_Type.AutoSize = true;
            this.label_Type.BackColor = System.Drawing.Color.Transparent;
            this.label_Type.Font = new System.Drawing.Font("Raleway", 8.999999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Type.Location = new System.Drawing.Point(270, 208);
            this.label_Type.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_Type.Name = "label_Type";
            this.label_Type.Size = new System.Drawing.Size(43, 18);
            this.label_Type.TabIndex = 18;
            this.label_Type.Text = "Type";
            // 
            // label_Maat
            // 
            this.label_Maat.AutoSize = true;
            this.label_Maat.BackColor = System.Drawing.Color.Transparent;
            this.label_Maat.Font = new System.Drawing.Font("Raleway", 8.999999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Maat.Location = new System.Drawing.Point(36, 267);
            this.label_Maat.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_Maat.Name = "label_Maat";
            this.label_Maat.Size = new System.Drawing.Size(45, 18);
            this.label_Maat.TabIndex = 19;
            this.label_Maat.Text = "Maat";
            // 
            // label_Aantal
            // 
            this.label_Aantal.AutoSize = true;
            this.label_Aantal.BackColor = System.Drawing.Color.Transparent;
            this.label_Aantal.Font = new System.Drawing.Font("Raleway", 8.999999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Aantal.Location = new System.Drawing.Point(270, 267);
            this.label_Aantal.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_Aantal.Name = "label_Aantal";
            this.label_Aantal.Size = new System.Drawing.Size(56, 18);
            this.label_Aantal.TabIndex = 20;
            this.label_Aantal.Text = "Aantal";
            // 
            // label_Prijs
            // 
            this.label_Prijs.AutoSize = true;
            this.label_Prijs.BackColor = System.Drawing.Color.Transparent;
            this.label_Prijs.Font = new System.Drawing.Font("Raleway", 8.999999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Prijs.Location = new System.Drawing.Point(160, 323);
            this.label_Prijs.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_Prijs.Name = "label_Prijs";
            this.label_Prijs.Size = new System.Drawing.Size(38, 18);
            this.label_Prijs.TabIndex = 21;
            this.label_Prijs.Text = "Prijs";
            // 
            // button_Toevoegen
            // 
            this.button_Toevoegen.BackColor = System.Drawing.Color.Gainsboro;
            this.button_Toevoegen.FlatAppearance.BorderColor = System.Drawing.Color.Aqua;
            this.button_Toevoegen.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button_Toevoegen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Toevoegen.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Toevoegen.Location = new System.Drawing.Point(204, 432);
            this.button_Toevoegen.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button_Toevoegen.Name = "button_Toevoegen";
            this.button_Toevoegen.Size = new System.Drawing.Size(160, 46);
            this.button_Toevoegen.TabIndex = 22;
            this.button_Toevoegen.Text = "Toevoegen";
            this.button_Toevoegen.UseVisualStyleBackColor = false;
            this.button_Toevoegen.Click += new System.EventHandler(this.button_Toevoegen_Click);
            // 
            // label_text
            // 
            this.label_text.AutoSize = true;
            this.label_text.BackColor = System.Drawing.Color.Transparent;
            this.label_text.ForeColor = System.Drawing.Color.Green;
            this.label_text.Location = new System.Drawing.Point(35, 398);
            this.label_text.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_text.Name = "label_text";
            this.label_text.Size = new System.Drawing.Size(0, 17);
            this.label_text.TabIndex = 23;
            // 
            // BackIcon
            // 
            this.BackIcon.BackColor = System.Drawing.Color.Transparent;
            this.BackIcon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BackIcon.Image = ((System.Drawing.Image)(resources.GetObject("BackIcon.Image")));
            this.BackIcon.Location = new System.Drawing.Point(16, 78);
            this.BackIcon.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BackIcon.Name = "BackIcon";
            this.BackIcon.Size = new System.Drawing.Size(47, 47);
            this.BackIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BackIcon.TabIndex = 24;
            this.BackIcon.TabStop = false;
            this.BackIcon.Click += new System.EventHandler(this.BackIcon_Click);
            // 
            // NieuwProduct
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(551, 519);
            this.Controls.Add(this.BackIcon);
            this.Controls.Add(this.label_text);
            this.Controls.Add(this.button_Toevoegen);
            this.Controls.Add(this.label_Prijs);
            this.Controls.Add(this.label_Aantal);
            this.Controls.Add(this.label_Maat);
            this.Controls.Add(this.label_Type);
            this.Controls.Add(this.label_Merk);
            this.Controls.Add(this.textbox_Prijs);
            this.Controls.Add(this.textbox_Aantal);
            this.Controls.Add(this.textbox_Maat);
            this.Controls.Add(this.textbox_Type);
            this.Controls.Add(this.textbox_Merk);
            this.Controls.Add(this.label_NieuwProduct);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "NieuwProduct";
            this.Text = "NieuwProduct";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ExitIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinimizeIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BackIcon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox ExitIcon;
        private System.Windows.Forms.PictureBox MinimizeIcon;
        private System.Windows.Forms.Label label_NieuwProduct;
        private System.Windows.Forms.TextBox textbox_Merk;
        private System.Windows.Forms.TextBox textbox_Type;
        private System.Windows.Forms.TextBox textbox_Maat;
        private System.Windows.Forms.TextBox textbox_Aantal;
        private System.Windows.Forms.TextBox textbox_Prijs;
        private System.Windows.Forms.Label label_Merk;
        private System.Windows.Forms.Label label_Type;
        private System.Windows.Forms.Label label_Maat;
        private System.Windows.Forms.Label label_Aantal;
        private System.Windows.Forms.Label label_Prijs;
        private System.Windows.Forms.Button button_Toevoegen;
        private System.Windows.Forms.Label label_text;
        private System.Windows.Forms.PictureBox BackIcon;
    }
}