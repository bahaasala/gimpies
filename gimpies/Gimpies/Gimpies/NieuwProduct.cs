﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Gimpies
{
    public partial class NieuwProduct : Form
    {
        List<string[]> data;
        public NieuwProduct()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
            data = getdata();
        }
        int mouseX = 0, mouseY = 0;
        bool mouseDown;


        //Icons :)
        private void ExitIcon_Click(object sender, EventArgs e)
        {
           this.Close();
        }
        private void MinimizeIcon_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private void BackIcon_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        //PanelMove
        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;
        }
        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }
        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                mouseX = MousePosition.X - 200;
                mouseY = MousePosition.Y - 40;
                this.SetDesktopLocation(mouseX, mouseY);
            }
        }


        private void button_Toevoegen_Click(object sender, EventArgs e)
        {
            
            try
            {
                if (textbox_Merk.Text == ""  && textbox_Type.Text == "" &&
                    textbox_Maat.Text == "" && textbox_Aantal.Text == "" &&
                    textbox_Prijs.Text == "")
                {
                    MessageBox.Show("Vul informatie in:");
                }
                else
                {

                }
                int nieuw = Convert.ToInt32(textbox_Maat.Text + textbox_Aantal.Text);

                string[] item = new string[5];
                item[0] = textbox_Merk.Text;
                item[1] = textbox_Type.Text;
                item[2] = textbox_Maat.Text;
                item[3] = textbox_Aantal.Text;
                item[4] = textbox_Prijs.Text;
                data.Add(item);
                savedata(data);
                label_text.Text = "Het nieuwe product is toegevoegd";
                textbox_Merk.Clear();
                textbox_Type.Clear();
                textbox_Maat.Clear();
                textbox_Aantal.Clear();
                textbox_Prijs.Clear();
                
            }
            catch (FormatException)
            {
                MessageBox.Show("Bij Maat,Aantal en Prijs alleen maar cijfers invullen aub");
                textbox_Maat.Clear();
                textbox_Aantal.Clear();
                textbox_Prijs.Clear();
            }
            catch (OverflowException)
            {
                MessageBox.Show("getal te groot");
                textbox_Maat.Clear();
                textbox_Aantal.Clear();
                textbox_Prijs.Clear();
            }
            catch
            {
                MessageBox.Show("unknown error");
            }
        }


        //The data
        public List<string[]> getdata()
        {
            string filePath = @"D:\School(ICT)\AO\LE-02-Gimpies\Opdracht\data\Voorraad.csv";
            StreamReader sr = new StreamReader(filePath);
            var lines = new List<string[]>();

            while (!sr.EndOfStream)
            {
                string[] Line = sr.ReadLine().Split(',');
                lines.Add(Line);
            }
            sr.Close();
            return lines;
        }

        public void savedata(List<string[]> data)
        {
            using (StreamWriter outfile = new StreamWriter(@"D:\School(ICT)\AO\LE-02-Gimpies\Opdracht\data\Voorraad.csv"))
            {
                for (int x = 0; x < data.Count; x++)
                {
                    string content = "";
                    for (int y = 0; y < data[x].Length - 1; y++)
                    {
                        content += data[x][y].ToString() + ",";
                    }
                    content += data[x][4].ToString();
                    outfile.WriteLine(content);
                }
            }
        }
    }
}
