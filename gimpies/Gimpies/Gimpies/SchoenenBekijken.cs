﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Gimpies
{
    public partial class SchoenenBekijken : Form
    {
        List<string[]> data;
        public SchoenenBekijken()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
            data = getdata();
            FillGrid(data);
        }

        int mouseX = 0, mouseY = 0;
        bool mouseDown;


        //Icons :)
        private void ExitIcon_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void MinimizeIcon_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private void BackIcon_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        //PanelMove
        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;
        }
        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }
        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                mouseX = MousePosition.X - 300;
                mouseY = MousePosition.Y - 40;
                this.SetDesktopLocation(mouseX, mouseY);
            }
        }


        public void FillGrid(List<string[]> data)
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("Merk");
            dt.Columns.Add("Type");
            dt.Columns.Add("Maat");
            dt.Columns.Add("Aantal");
            dt.Columns.Add("Prijs");

            for (int i = 1; i < data.Count; i++)
            {
                dt.Rows.Add(data[i][0], data[i][1], data[i][2], data[i][3], "€" + data[i][4]);
            }

            grid1.DataSource = dt;

            foreach (DataGridViewColumn column in grid1.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
        }


        //The data
        public List<string[]> getdata()
        {
            string filePath = @"D:\School(ICT)\AO\LE-02-Gimpies\Opdracht\data\Voorraad.csv";
            StreamReader sr = new StreamReader(filePath);
            var lines = new List<string[]>();

            while (!sr.EndOfStream)
            {
                string[] Line = sr.ReadLine().Split(',');
                lines.Add(Line);
            }

            return lines;
        }
    }
}
