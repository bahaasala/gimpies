﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gimpies
{
    public partial class Hmenu : Form
    {
        public Hmenu(string rol)
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
            SetRole(rol);
        }

        int mouseX = 0, mouseY = 0;
        bool mouseDown;


        //Icons :)
        private void ExitIcon_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MinimizeIcon_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void LogoutIcon_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        //PanelMove
        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;
        }
        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }
        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                mouseX = MousePosition.X - 200;
                mouseY = MousePosition.Y - 40;
                this.SetDesktopLocation(mouseX, mouseY);
            }
        }


        //Buttons
        private void BekijkenButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            SchoenenBekijken f3 = new SchoenenBekijken();
            f3.ShowDialog();
            this.Show();
        }
        private void ToevoegenButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            SchoenenToevoegen f4 = new SchoenenToevoegen();
            f4.ShowDialog();
            this.Show();
        }

        private void NieuwProduct_Click(object sender, EventArgs e)
        {
            this.Hide();
            NieuwProduct f6 = new NieuwProduct();
            f6.ShowDialog();
            this.Show();
        }

        private void button_DeGebruikers_Click(object sender, EventArgs e)
        {
            this.Hide();
            Users_Admin f8 = new Users_Admin();
            f8.ShowDialog();
            this.Show();
        }

        private void VerwijderenButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            SchoenenVerwijderen f5 = new SchoenenVerwijderen();
            f5.ShowDialog();
            this.Show();
        }


        public void SetRole(string rol)
        {
            if (rol == "admin")
            {

            }
            if (rol == "user")
            {
                button_DeGebruikers.Hide();
                UsersIcon.Hide();
            }

        }
    }
}
