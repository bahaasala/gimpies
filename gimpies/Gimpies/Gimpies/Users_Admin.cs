﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Gimpies
{
    public partial class Users_Admin : Form
    {
        List<string[]> data;
        List<string[]> data2;
        public Users_Admin()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
            data = getusers();
            data2 = getadmins();
            FillGrid(data);
            FillGrid1(data2);
            panel_nieuw.Hide();
            panel_wijzigen.Hide();
            panel_verwijderen.Hide();
            BackIcon2.Hide();
        }
        int mouseX = 0, mouseY = 0;
        bool mouseDown;
        int indexrow;


        //Icons :)
        private void ExitIcon_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void MinimizeIcon_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private void BackIcon_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void BackIcon2_Click(object sender, EventArgs e)
        {
            panel_wijzigen.Hide();
            panel_nieuw.Hide();
            panel_verwijderen.Hide();
            BackIcon2.Hide();
        }


        //PanelMove
        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;
        }
        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }
        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                mouseX = MousePosition.X - 300;
                mouseY = MousePosition.Y - 40;
                this.SetDesktopLocation(mouseX, mouseY);
            }
        }


        private void Users_Admin_Load(object sender, EventArgs e)
        {
            comboBox1.Items.Add("Admin");
            comboBox1.Items.Add("User");
        }


        //Buttons
        private void button_Admins_Click(object sender, EventArgs e)
        {
            GridUsers.Hide();
            GridAdmins.Show();
            button_wijzigen.BringToFront();
            button_Verwijderen.BringToFront();
        }

        private void button_Users_Click(object sender, EventArgs e)
        {
            GridAdmins.Hide();
            GridUsers.Show();
            button_wijzigen2.BringToFront();
            button_Verwijderen2.BringToFront();
        }

        private void button_GebruikerToevoegen_Click(object sender, EventArgs e)
        {
            panel_nieuw.Show();
            panel_nieuw.BringToFront();
            BackIcon2.Show();
            label3.Text = "";
        }

        private void button_GebuikerVerwijderen_Click(object sender, EventArgs e)
        {
            panel_verwijderen.Show();
            panel_verwijderen.BringToFront();
            BackIcon2.Show();
        }

        private void button1_GegevensWijzigen_Click(object sender, EventArgs e)
        {
            panel_wijzigen.Show();
            panel_wijzigen.BringToFront();
            BackIcon2.Show();
            label2.Text = "";
        }
        
        //Toevoegen
        private void button_Toevoegen_Click(object sender, EventArgs e)
        {
            if (textbox_Gebruikersnaam.Text == "")
            {
                MessageBox.Show("Maak een gebruikersnaam aan!");
            }
            else if (textbox_Wachtwoord.Text == "")
            {
                MessageBox.Show("Maak een wachtwoord aan!");
            }
            else if (comboBox1.SelectedIndex == comboBox1.FindStringExact(""))
            {
                MessageBox.Show("Kies Admin of User!!");
            }
            else if (comboBox1.SelectedIndex == comboBox1.FindStringExact("Admin"))
            {
                string[] item = new string[2];
                item[0] = textbox_Gebruikersnaam.Text;
                item[1] = textbox_Wachtwoord.Text;
                data2.Add(item);
                saveadmins(data2);
                FillGrid1(data2);
                label3.Text = "De gebruiker is toegevoegd";
                textbox_Gebruikersnaam.Clear();
                textbox_Wachtwoord.Clear();
            }
            else if (comboBox1.SelectedIndex == comboBox1.FindStringExact("User"))
            {
                string[] item = new string[2];
                item[0] = textbox_Gebruikersnaam.Text;
                item[1] = textbox_Wachtwoord.Text;
                data.Add(item);
                saveusers(data);
                FillGrid(data);
                label3.Text = "De gebruiker is toegevoegd";
                textbox_Gebruikersnaam.Clear();
                textbox_Wachtwoord.Clear();
            }
            else
            {

            }
        }

        //Verwijderen
        private void button_Verwijderen_Click(object sender, EventArgs e)
        {
            data2.RemoveAt(GridAdmins.CurrentCell.RowIndex);
            int rowIndex1 = GridAdmins.CurrentCell.RowIndex;
            GridAdmins.Rows.RemoveAt(rowIndex1);
            textbox_wijzig1.Clear();
            textbox_wijzig2.Clear();
            saveadmins(data2);
            FillGrid1(data2);
        }

        private void button_Verwijderen2_Click(object sender, EventArgs e)
        {
            data.RemoveAt(GridUsers.CurrentCell.RowIndex);
            int rowIndex = GridUsers.CurrentCell.RowIndex;
            GridUsers.Rows.RemoveAt(rowIndex);
            textbox_wijzig1.Clear();
            textbox_wijzig2.Clear();
            saveusers(data);
            FillGrid(data);
        }

        //Wijzigen
        private void button_wijzigen_Click(object sender, EventArgs e)
        {
            try
            {
                if (textbox_wijzig1.Text == "" && textbox_wijzig2.Text == ""
               || textbox_wijzig1.Text == "" || textbox_wijzig2.Text == "")
                {
                    MessageBox.Show("Selecteer eerst een rij!");
                }
                else
                {
                    DataGridViewRow newDataRow = GridAdmins.Rows[indexrow];
                    newDataRow.Cells[0].Value = textbox_wijzig1.Text;
                    newDataRow.Cells[1].Value = textbox_wijzig2.Text;

                    string voorraad = Convert.ToString(textbox_wijzig1.Text);
                    data2[GridAdmins.CurrentCell.RowIndex][0] = Convert.ToString(voorraad);
                    
                    string voorraad1 = Convert.ToString(textbox_wijzig2.Text);
                    data2[GridAdmins.CurrentCell.RowIndex][1] = Convert.ToString(voorraad1);

                    textbox_wijzig1.Text = "";
                    textbox_wijzig2.Text = "";
                    label2.Text = "De gegevens zijn gewijzigd";

                    saveadmins(data2);
                    FillGrid1(data2);
                }
            }
            catch { }
        }

        private void button_wijzigen2_Click(object sender, EventArgs e)
        {
            try
            {
                if (textbox_wijzig1.Text == "" && textbox_wijzig2.Text == ""
                || textbox_wijzig1.Text == "" || textbox_wijzig2.Text == "")
                {
                    MessageBox.Show("Selecteer eerst een rij!");
                }
                else
                {
                    DataGridViewRow newDataRow = GridUsers.Rows[indexrow];
                    newDataRow.Cells[0].Value = textbox_wijzig1.Text;
                    newDataRow.Cells[1].Value = textbox_wijzig2.Text;


                    string voorraad3 = Convert.ToString(textbox_wijzig1.Text);
                    data[GridUsers.CurrentCell.RowIndex][0] = Convert.ToString(voorraad3);
                    
                    string voorraad4 = Convert.ToString(textbox_wijzig2.Text);
                    data[GridUsers.CurrentCell.RowIndex][1] = Convert.ToString(voorraad4);

                    textbox_wijzig1.Text = "";
                    textbox_wijzig2.Text = "";
                    label2.Text = "De gegevens zijn gewijzigd";

                    saveusers(data);
                    FillGrid(data);
                }
            }
            catch { }
        }

        private void GridAdmins_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                indexrow = e.RowIndex;
                DataGridViewRow row = GridAdmins.Rows[indexrow];
                textbox_wijzig1.Text = row.Cells[0].Value.ToString();
                textbox_wijzig2.Text = row.Cells[1].Value.ToString();
            }
            catch { }
        }

        private void GridUsers_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                indexrow = e.RowIndex;
                DataGridViewRow row = GridUsers.Rows[indexrow];
                textbox_wijzig1.Text = row.Cells[0].Value.ToString();
                textbox_wijzig2.Text = row.Cells[1].Value.ToString();
            }
            catch { }
        }


        //FillGrids
        public void FillGrid(List<string[]> data)
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("Gebruikersnaam");
            dt.Columns.Add("Wachtwoord");

            for (int i = 0; i < data.Count; i++)
            {
                dt.Rows.Add(data[i][0], data[i][1]);
            }

            GridUsers.DataSource = dt;

            foreach (DataGridViewColumn column in GridUsers.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
        }
        
        public void FillGrid1(List<string[]> data2)
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("Gebruikersnaam");
            dt.Columns.Add("Wachtwoord");

            for (int i = 0; i < data2.Count; i++)
            {
                dt.Rows.Add(data2[i][0], data2[i][1]);
            }

            GridAdmins.DataSource = dt;

            foreach (DataGridViewColumn column in GridAdmins.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
        }

        
        //The data
        public List<string[]> getusers()
        {
            string filePath = @"D:\School(ICT)\AO\LE-02-Gimpies\Opdracht\data\Users.csv";
            StreamReader sr = new StreamReader(filePath);
            var lines = new List<string[]>();

            while (!sr.EndOfStream)
            {
                string[] Line = sr.ReadLine().Split(',');
                lines.Add(Line);
            }
            sr.Close();
            return lines;
        }

        public void saveusers(List<string[]> data)
        {
            using (StreamWriter outfile = new StreamWriter(@"D:\School(ICT)\AO\LE-02-Gimpies\Opdracht\data\Users.csv"))
            {
                for (int x = 0; x < data.Count; x++)
                {
                    string content = "";
                    for (int y = 0; y < data[x].Length - 1; y++)
                    {
                        content += data[x][y].ToString() + ",";
                    }
                    content += data[x][1].ToString();
                    outfile.WriteLine(content);
                }
            }
        }

        public List<string[]> getadmins()
        {
            string filePath = @"D:\School(ICT)\AO\LE-02-Gimpies\Opdracht\data\Admins.csv";
            StreamReader sr = new StreamReader(filePath);
            var lines = new List<string[]>();

            while (!sr.EndOfStream)
            {
                string[] Line = sr.ReadLine().Split(',');
                lines.Add(Line);
            }
            sr.Close();
            return lines;
        }

        public void saveadmins(List<string[]> data2)
        {
            using (StreamWriter outfile = new StreamWriter(@"D:\School(ICT)\AO\LE-02-Gimpies\Opdracht\data\Admins.csv"))
            {
                for (int x = 0; x < data2.Count; x++)
                {
                    string content = "";
                    for (int y = 0; y < data2[x].Length - 1; y++)
                    {
                        content += data2[x][y].ToString() + ",";
                    }
                    content += data2[x][1].ToString();
                    outfile.WriteLine(content);
                }
            }
        }
    }
}
