﻿namespace Gimpies
{
    partial class SchoenenBekijken
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SchoenenBekijken));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.MinimizeIcon = new System.Windows.Forms.PictureBox();
            this.ExitIcon = new System.Windows.Forms.PictureBox();
            this.grid1 = new System.Windows.Forms.DataGridView();
            this.BackIcon = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.shoes5 = new System.Windows.Forms.PictureBox();
            this.shoes2 = new System.Windows.Forms.PictureBox();
            this.shoes4 = new System.Windows.Forms.PictureBox();
            this.shoes6 = new System.Windows.Forms.PictureBox();
            this.shoes1 = new System.Windows.Forms.PictureBox();
            this.shoes3 = new System.Windows.Forms.PictureBox();
            this.shoes11 = new System.Windows.Forms.PictureBox();
            this.shoes8 = new System.Windows.Forms.PictureBox();
            this.shoes10 = new System.Windows.Forms.PictureBox();
            this.shoes12 = new System.Windows.Forms.PictureBox();
            this.shoes7 = new System.Windows.Forms.PictureBox();
            this.shoes9 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MinimizeIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExitIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BackIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shoes5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shoes2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shoes4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shoes6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shoes1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shoes3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shoes11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shoes8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shoes10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shoes12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shoes7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shoes9)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.MinimizeIcon);
            this.panel1.Controls.Add(this.ExitIcon);
            this.panel1.Location = new System.Drawing.Point(-3, 1);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(871, 107);
            this.panel1.TabIndex = 10;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseUp);
            // 
            // MinimizeIcon
            // 
            this.MinimizeIcon.BackColor = System.Drawing.Color.Transparent;
            this.MinimizeIcon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MinimizeIcon.Image = ((System.Drawing.Image)(resources.GetObject("MinimizeIcon.Image")));
            this.MinimizeIcon.Location = new System.Drawing.Point(748, 33);
            this.MinimizeIcon.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MinimizeIcon.Name = "MinimizeIcon";
            this.MinimizeIcon.Size = new System.Drawing.Size(44, 44);
            this.MinimizeIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.MinimizeIcon.TabIndex = 10;
            this.MinimizeIcon.TabStop = false;
            this.MinimizeIcon.Click += new System.EventHandler(this.MinimizeIcon_Click);
            // 
            // ExitIcon
            // 
            this.ExitIcon.BackColor = System.Drawing.Color.Transparent;
            this.ExitIcon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ExitIcon.Image = ((System.Drawing.Image)(resources.GetObject("ExitIcon.Image")));
            this.ExitIcon.Location = new System.Drawing.Point(800, 30);
            this.ExitIcon.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ExitIcon.Name = "ExitIcon";
            this.ExitIcon.Size = new System.Drawing.Size(48, 48);
            this.ExitIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ExitIcon.TabIndex = 9;
            this.ExitIcon.TabStop = false;
            this.ExitIcon.Click += new System.EventHandler(this.ExitIcon_Click);
            // 
            // grid1
            // 
            this.grid1.AllowUserToAddRows = false;
            this.grid1.AllowUserToDeleteRows = false;
            this.grid1.AllowUserToResizeColumns = false;
            this.grid1.AllowUserToResizeRows = false;
            this.grid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Raleway", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.grid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grid1.DefaultCellStyle = dataGridViewCellStyle2;
            this.grid1.GridColor = System.Drawing.Color.LightBlue;
            this.grid1.Location = new System.Drawing.Point(91, 156);
            this.grid1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grid1.MultiSelect = false;
            this.grid1.Name = "grid1";
            this.grid1.ReadOnly = true;
            this.grid1.RowHeadersVisible = false;
            this.grid1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.grid1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grid1.Size = new System.Drawing.Size(689, 332);
            this.grid1.TabIndex = 11;
            // 
            // BackIcon
            // 
            this.BackIcon.BackColor = System.Drawing.Color.Transparent;
            this.BackIcon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BackIcon.Image = ((System.Drawing.Image)(resources.GetObject("BackIcon.Image")));
            this.BackIcon.Location = new System.Drawing.Point(32, 87);
            this.BackIcon.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BackIcon.Name = "BackIcon";
            this.BackIcon.Size = new System.Drawing.Size(47, 47);
            this.BackIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BackIcon.TabIndex = 19;
            this.BackIcon.TabStop = false;
            this.BackIcon.Click += new System.EventHandler(this.BackIcon_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(92, 480);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(65, 65);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 20;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(476, 480);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(65, 65);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 21;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(243, 480);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(65, 65);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 22;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(321, 480);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(65, 65);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 23;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(168, 487);
            this.pictureBox5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(65, 65);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 24;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(400, 489);
            this.pictureBox6.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(65, 65);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 25;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(635, 487);
            this.pictureBox8.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(65, 65);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox8.TabIndex = 30;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox11.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox11.Image")));
            this.pictureBox11.Location = new System.Drawing.Point(556, 480);
            this.pictureBox11.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(65, 65);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox11.TabIndex = 26;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox12.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox12.Image")));
            this.pictureBox12.Location = new System.Drawing.Point(712, 480);
            this.pictureBox12.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(65, 65);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox12.TabIndex = 28;
            this.pictureBox12.TabStop = false;
            // 
            // shoes5
            // 
            this.shoes5.BackColor = System.Drawing.Color.Transparent;
            this.shoes5.Image = ((System.Drawing.Image)(resources.GetObject("shoes5.Image")));
            this.shoes5.Location = new System.Drawing.Point(13, 393);
            this.shoes5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.shoes5.Name = "shoes5";
            this.shoes5.Size = new System.Drawing.Size(65, 65);
            this.shoes5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.shoes5.TabIndex = 36;
            this.shoes5.TabStop = false;
            // 
            // shoes2
            // 
            this.shoes2.BackColor = System.Drawing.Color.Transparent;
            this.shoes2.Image = ((System.Drawing.Image)(resources.GetObject("shoes2.Image")));
            this.shoes2.Location = new System.Drawing.Point(13, 223);
            this.shoes2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.shoes2.Name = "shoes2";
            this.shoes2.Size = new System.Drawing.Size(65, 65);
            this.shoes2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.shoes2.TabIndex = 35;
            this.shoes2.TabStop = false;
            // 
            // shoes4
            // 
            this.shoes4.BackColor = System.Drawing.Color.Transparent;
            this.shoes4.Image = ((System.Drawing.Image)(resources.GetObject("shoes4.Image")));
            this.shoes4.Location = new System.Drawing.Point(13, 338);
            this.shoes4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.shoes4.Name = "shoes4";
            this.shoes4.Size = new System.Drawing.Size(65, 65);
            this.shoes4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.shoes4.TabIndex = 34;
            this.shoes4.TabStop = false;
            // 
            // shoes6
            // 
            this.shoes6.BackColor = System.Drawing.Color.Transparent;
            this.shoes6.Image = ((System.Drawing.Image)(resources.GetObject("shoes6.Image")));
            this.shoes6.Location = new System.Drawing.Point(13, 444);
            this.shoes6.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.shoes6.Name = "shoes6";
            this.shoes6.Size = new System.Drawing.Size(65, 65);
            this.shoes6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.shoes6.TabIndex = 32;
            this.shoes6.TabStop = false;
            // 
            // shoes1
            // 
            this.shoes1.BackColor = System.Drawing.Color.Transparent;
            this.shoes1.Image = ((System.Drawing.Image)(resources.GetObject("shoes1.Image")));
            this.shoes1.Location = new System.Drawing.Point(13, 170);
            this.shoes1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.shoes1.Name = "shoes1";
            this.shoes1.Size = new System.Drawing.Size(65, 65);
            this.shoes1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.shoes1.TabIndex = 31;
            this.shoes1.TabStop = false;
            // 
            // shoes3
            // 
            this.shoes3.BackColor = System.Drawing.Color.Transparent;
            this.shoes3.Image = ((System.Drawing.Image)(resources.GetObject("shoes3.Image")));
            this.shoes3.Location = new System.Drawing.Point(13, 281);
            this.shoes3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.shoes3.Name = "shoes3";
            this.shoes3.Size = new System.Drawing.Size(65, 65);
            this.shoes3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.shoes3.TabIndex = 33;
            this.shoes3.TabStop = false;
            // 
            // shoes11
            // 
            this.shoes11.BackColor = System.Drawing.Color.Transparent;
            this.shoes11.Image = ((System.Drawing.Image)(resources.GetObject("shoes11.Image")));
            this.shoes11.Location = new System.Drawing.Point(788, 393);
            this.shoes11.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.shoes11.Name = "shoes11";
            this.shoes11.Size = new System.Drawing.Size(65, 65);
            this.shoes11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.shoes11.TabIndex = 42;
            this.shoes11.TabStop = false;
            // 
            // shoes8
            // 
            this.shoes8.BackColor = System.Drawing.Color.Transparent;
            this.shoes8.Image = ((System.Drawing.Image)(resources.GetObject("shoes8.Image")));
            this.shoes8.Location = new System.Drawing.Point(788, 223);
            this.shoes8.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.shoes8.Name = "shoes8";
            this.shoes8.Size = new System.Drawing.Size(65, 65);
            this.shoes8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.shoes8.TabIndex = 41;
            this.shoes8.TabStop = false;
            // 
            // shoes10
            // 
            this.shoes10.BackColor = System.Drawing.Color.Transparent;
            this.shoes10.Image = ((System.Drawing.Image)(resources.GetObject("shoes10.Image")));
            this.shoes10.Location = new System.Drawing.Point(788, 338);
            this.shoes10.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.shoes10.Name = "shoes10";
            this.shoes10.Size = new System.Drawing.Size(65, 65);
            this.shoes10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.shoes10.TabIndex = 40;
            this.shoes10.TabStop = false;
            // 
            // shoes12
            // 
            this.shoes12.BackColor = System.Drawing.Color.Transparent;
            this.shoes12.Image = ((System.Drawing.Image)(resources.GetObject("shoes12.Image")));
            this.shoes12.Location = new System.Drawing.Point(788, 444);
            this.shoes12.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.shoes12.Name = "shoes12";
            this.shoes12.Size = new System.Drawing.Size(65, 65);
            this.shoes12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.shoes12.TabIndex = 38;
            this.shoes12.TabStop = false;
            // 
            // shoes7
            // 
            this.shoes7.BackColor = System.Drawing.Color.Transparent;
            this.shoes7.Image = ((System.Drawing.Image)(resources.GetObject("shoes7.Image")));
            this.shoes7.Location = new System.Drawing.Point(788, 170);
            this.shoes7.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.shoes7.Name = "shoes7";
            this.shoes7.Size = new System.Drawing.Size(65, 65);
            this.shoes7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.shoes7.TabIndex = 37;
            this.shoes7.TabStop = false;
            // 
            // shoes9
            // 
            this.shoes9.BackColor = System.Drawing.Color.Transparent;
            this.shoes9.Image = ((System.Drawing.Image)(resources.GetObject("shoes9.Image")));
            this.shoes9.Location = new System.Drawing.Point(788, 281);
            this.shoes9.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.shoes9.Name = "shoes9";
            this.shoes9.Size = new System.Drawing.Size(65, 65);
            this.shoes9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.shoes9.TabIndex = 39;
            this.shoes9.TabStop = false;
            // 
            // SchoenenBekijken
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(867, 642);
            this.Controls.Add(this.shoes11);
            this.Controls.Add(this.shoes8);
            this.Controls.Add(this.shoes10);
            this.Controls.Add(this.shoes12);
            this.Controls.Add(this.shoes7);
            this.Controls.Add(this.shoes9);
            this.Controls.Add(this.shoes5);
            this.Controls.Add(this.shoes2);
            this.Controls.Add(this.shoes4);
            this.Controls.Add(this.shoes6);
            this.Controls.Add(this.shoes1);
            this.Controls.Add(this.shoes3);
            this.Controls.Add(this.BackIcon);
            this.Controls.Add(this.grid1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox11);
            this.Controls.Add(this.pictureBox12);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "SchoenenBekijken";
            this.Text = "Form3";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MinimizeIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExitIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BackIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shoes5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shoes2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shoes4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shoes6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shoes1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shoes3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shoes11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shoes8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shoes10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shoes12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shoes7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shoes9)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox MinimizeIcon;
        private System.Windows.Forms.PictureBox ExitIcon;
        private System.Windows.Forms.PictureBox BackIcon;
        public System.Windows.Forms.DataGridView grid1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox shoes5;
        private System.Windows.Forms.PictureBox shoes2;
        private System.Windows.Forms.PictureBox shoes4;
        private System.Windows.Forms.PictureBox shoes6;
        private System.Windows.Forms.PictureBox shoes1;
        private System.Windows.Forms.PictureBox shoes3;
        private System.Windows.Forms.PictureBox shoes11;
        private System.Windows.Forms.PictureBox shoes8;
        private System.Windows.Forms.PictureBox shoes10;
        private System.Windows.Forms.PictureBox shoes12;
        private System.Windows.Forms.PictureBox shoes7;
        private System.Windows.Forms.PictureBox shoes9;
    }
}