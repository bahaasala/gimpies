﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Gimpies
{
    public partial class SchoenenVerwijderen : Form
    {
        List<string[]> data;
        public SchoenenVerwijderen()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
            data = getdata();
            FillGrid(data);
        }
        int mouseX = 0, mouseY = 0;
        bool mouseDown;


        //Icons :)
        private void ExitIcon_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void MinimizeIcon_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private void BackIcon_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        //PanelMove
        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;
        }
        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                mouseX = MousePosition.X - 300;
                mouseY = MousePosition.Y - 40;
                this.SetDesktopLocation(mouseX, mouseY);
            }
        }


        private void verwijderen_Click(object sender, EventArgs e)
        {
            try
            {
                int verwijderen = Convert.ToInt32(textbox_Aantal.Text);
                if (verwijderen < 0)
                {
                    MessageBox.Show("Alleen positieve waarde invullen");
                }
                else if (verwijderen > Convert.ToInt32(data[grid3.CurrentCell.RowIndex + 1][3]))
                {
                    MessageBox.Show("U kunt niet meer dan het aantal verwijderen ");
                    textbox_Aantal.Clear();
                }
                else
                {
                    int voorraad = Convert.ToInt32(data[grid3.CurrentCell.RowIndex + 1][3]);
                    voorraad = voorraad - verwijderen;
                    data[grid3.CurrentCell.RowIndex + 1][3] = Convert.ToString(voorraad);
                    savedata(data);
                    FillGrid(data);
                    textbox_Aantal.Clear();
                }
            }
            catch (FormatException)
            {
                MessageBox.Show("Alleen maar cijfers invullen aub");
            }
            catch (OverflowException)
            {
                MessageBox.Show("getal te groot");
            }
            catch
            {
                MessageBox.Show("unknown error");
            }
        }
        
        public void FillGrid(List<string[]> data)
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("Merk");
            dt.Columns.Add("Type");
            dt.Columns.Add("Maat");
            dt.Columns.Add("Aantal");
            dt.Columns.Add("Prijs");

            for (int i = 1; i < data.Count; i++)
            {
                dt.Rows.Add(data[i][0], data[i][1], data[i][2], data[i][3], "€" + data[i][4]);
            }

            grid3.DataSource = dt;

            foreach (DataGridViewColumn column in grid3.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
        }


        //The data
        public List<string[]> getdata()
        {
            string filePath = @"D:\School(ICT)\AO\LE-02-Gimpies\Opdracht\data\Voorraad.csv";
            StreamReader sr = new StreamReader(filePath);
            var lines = new List<string[]>();

            while (!sr.EndOfStream)
            {
                string[] Line = sr.ReadLine().Split(',');
                lines.Add(Line);
            }
            sr.Close();
            return lines;
        }

        public void savedata(List<string[]> data)
        {
            using (StreamWriter outfile = new StreamWriter(@"D:\School(ICT)\AO\LE-02-Gimpies\Opdracht\data\Voorraad.csv"))
            {
                for (int x = 0; x < data.Count; x++)
                {
                    string content = "";
                    for (int y = 0; y < data[x].Length - 1; y++)
                    {
                        content += data[x][y].ToString() + ",";
                    }
                    content += data[x][4].ToString();
                    outfile.WriteLine(content);
                }
            }
        }
    }
}
