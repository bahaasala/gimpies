﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Gimpies
{
    public partial class InlogPagina : Form
    {
        List<string[]> data;
        List<string[]> data2;
        int attempts = 0;
        public InlogPagina()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
            data = getusers();
            data2 = getadmins();
        }

        int mouseX = 0, mouseY = 0;
        bool mouseDown;

        //Icons :)
        private void ExitIcon_Click(object sender, EventArgs e)
        {
           this.Close();
        }

        private void MinimizeIcon_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }


        //PanelMove
        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;
        }
        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }
        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                mouseX = MousePosition.X - 300;
                mouseY = MousePosition.Y - 40;
                this.SetDesktopLocation(mouseX, mouseY);
            }
        }
        

        private void button_Inloggen_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < data2.Count; i++)
            {
                if (textbox_Gebruikersnaam.Text == data2[i][0] && textbox_Wachtwoord.Text == data2[i][1])
                {
                    this.Hide();
                    Hmenu f22 = new Hmenu("admin");
                    f22.ShowDialog();
                    i = data.Count + 1;
                    data = getusers();
                    data2 = getadmins();
                    textbox_Gebruikersnaam.Clear();
                    textbox_Wachtwoord.Clear();
                    attempts = 0;
                    label3.Text = "";
                    label4.Text = "";
                    this.Show();
                }
            }
            for (int i = 0; i < data.Count; i++)
            {
                if (textbox_Gebruikersnaam.Text == data[i][0] && textbox_Wachtwoord.Text == data[i][1])
                {
                    this.Hide();
                    Hmenu f2 = new Hmenu("user");
                    f2.ShowDialog();
                    i = data2.Count + 1;
                    textbox_Gebruikersnaam.Clear();
                    textbox_Wachtwoord.Clear();
                    attempts = 0;
                    label3.Text = "";
                    label4.Text = "";
                    this.Show();
                }
            }
            if (textbox_Wachtwoord.Text != "")
            {
                attempts++;
                label3.Text = "Helaas kloppen je inloggegevens niet.";
                label4.Text = "Probeer het nog een keer:  " + attempts + "out of 3";
            }
            if (attempts == 3)
            {
                this.Close();
            }
        }
        

        //The data
        public List<string[]> getusers()
        {
            string filePath = @"D:\School(ICT)\AO\LE-02-Gimpies\Opdracht\data\Users.csv";
            StreamReader sr = new StreamReader(filePath);
            var lines = new List<string[]>();

            while (!sr.EndOfStream)
            {
                string[] Line = sr.ReadLine().Split(',');
                lines.Add(Line);
            }
            sr.Close();
            return lines;
        }

        public List<string[]> getadmins()
        {
            string filePath = @"D:\School(ICT)\AO\LE-02-Gimpies\Opdracht\data\Admins.csv";
            StreamReader sr = new StreamReader(filePath);
            var lines = new List<string[]>();

            while (!sr.EndOfStream)
            {
                string[] Line = sr.ReadLine().Split(',');
                lines.Add(Line);
            }
            sr.Close();
            return lines;
        }
    }
}
