﻿namespace Gimpies
{
    partial class InlogPagina
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InlogPagina));
            this.label_Gebruikersnaam = new System.Windows.Forms.Label();
            this.label_Wachtwoord = new System.Windows.Forms.Label();
            this.textbox_Gebruikersnaam = new System.Windows.Forms.TextBox();
            this.textbox_Wachtwoord = new System.Windows.Forms.TextBox();
            this.button_Inloggen = new System.Windows.Forms.Button();
            this.ExitIcon = new System.Windows.Forms.PictureBox();
            this.MinimizeIcon = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ExitIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinimizeIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // label_Gebruikersnaam
            // 
            this.label_Gebruikersnaam.AutoSize = true;
            this.label_Gebruikersnaam.BackColor = System.Drawing.Color.Transparent;
            this.label_Gebruikersnaam.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Gebruikersnaam.Location = new System.Drawing.Point(359, 400);
            this.label_Gebruikersnaam.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_Gebruikersnaam.Name = "label_Gebruikersnaam";
            this.label_Gebruikersnaam.Size = new System.Drawing.Size(159, 22);
            this.label_Gebruikersnaam.TabIndex = 0;
            this.label_Gebruikersnaam.Text = "Gebruikersnaam";
            // 
            // label_Wachtwoord
            // 
            this.label_Wachtwoord.AutoSize = true;
            this.label_Wachtwoord.BackColor = System.Drawing.Color.Transparent;
            this.label_Wachtwoord.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Wachtwoord.Location = new System.Drawing.Point(364, 437);
            this.label_Wachtwoord.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_Wachtwoord.Name = "label_Wachtwoord";
            this.label_Wachtwoord.Size = new System.Drawing.Size(128, 22);
            this.label_Wachtwoord.TabIndex = 1;
            this.label_Wachtwoord.Text = "Wachtwoord";
            // 
            // textbox_Gebruikersnaam
            // 
            this.textbox_Gebruikersnaam.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textbox_Gebruikersnaam.Location = new System.Drawing.Point(537, 400);
            this.textbox_Gebruikersnaam.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textbox_Gebruikersnaam.Name = "textbox_Gebruikersnaam";
            this.textbox_Gebruikersnaam.Size = new System.Drawing.Size(227, 30);
            this.textbox_Gebruikersnaam.TabIndex = 2;
            // 
            // textbox_Wachtwoord
            // 
            this.textbox_Wachtwoord.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textbox_Wachtwoord.Location = new System.Drawing.Point(536, 437);
            this.textbox_Wachtwoord.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textbox_Wachtwoord.Name = "textbox_Wachtwoord";
            this.textbox_Wachtwoord.Size = new System.Drawing.Size(227, 28);
            this.textbox_Wachtwoord.TabIndex = 3;
            this.textbox_Wachtwoord.UseSystemPasswordChar = true;
            // 
            // button_Inloggen
            // 
            this.button_Inloggen.BackColor = System.Drawing.Color.DarkTurquoise;
            this.button_Inloggen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button_Inloggen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_Inloggen.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.button_Inloggen.FlatAppearance.BorderSize = 2;
            this.button_Inloggen.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.button_Inloggen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Inloggen.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Inloggen.Location = new System.Drawing.Point(597, 516);
            this.button_Inloggen.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button_Inloggen.Name = "button_Inloggen";
            this.button_Inloggen.Size = new System.Drawing.Size(92, 50);
            this.button_Inloggen.TabIndex = 4;
            this.button_Inloggen.Text = "Inloggen";
            this.button_Inloggen.UseVisualStyleBackColor = false;
            this.button_Inloggen.Click += new System.EventHandler(this.button_Inloggen_Click);
            // 
            // ExitIcon
            // 
            this.ExitIcon.BackColor = System.Drawing.Color.Transparent;
            this.ExitIcon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ExitIcon.Image = ((System.Drawing.Image)(resources.GetObject("ExitIcon.Image")));
            this.ExitIcon.Location = new System.Drawing.Point(760, 15);
            this.ExitIcon.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ExitIcon.Name = "ExitIcon";
            this.ExitIcon.Size = new System.Drawing.Size(48, 48);
            this.ExitIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ExitIcon.TabIndex = 5;
            this.ExitIcon.TabStop = false;
            this.ExitIcon.Click += new System.EventHandler(this.ExitIcon_Click);
            // 
            // MinimizeIcon
            // 
            this.MinimizeIcon.BackColor = System.Drawing.Color.Transparent;
            this.MinimizeIcon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MinimizeIcon.Image = ((System.Drawing.Image)(resources.GetObject("MinimizeIcon.Image")));
            this.MinimizeIcon.Location = new System.Drawing.Point(708, 18);
            this.MinimizeIcon.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MinimizeIcon.Name = "MinimizeIcon";
            this.MinimizeIcon.Size = new System.Drawing.Size(44, 44);
            this.MinimizeIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.MinimizeIcon.TabIndex = 6;
            this.MinimizeIcon.TabStop = false;
            this.MinimizeIcon.Click += new System.EventHandler(this.MinimizeIcon_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Location = new System.Drawing.Point(-1, -2);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(828, 92);
            this.panel1.TabIndex = 7;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseUp);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(532, 470);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 17);
            this.label3.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(532, 487);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 17);
            this.label4.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(537, 475);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 17);
            this.label5.TabIndex = 10;
            // 
            // InlogPagina
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(824, 642);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.MinimizeIcon);
            this.Controls.Add(this.ExitIcon);
            this.Controls.Add(this.button_Inloggen);
            this.Controls.Add(this.textbox_Wachtwoord);
            this.Controls.Add(this.textbox_Gebruikersnaam);
            this.Controls.Add(this.label_Wachtwoord);
            this.Controls.Add(this.label_Gebruikersnaam);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "InlogPagina";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.ExitIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinimizeIcon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_Gebruikersnaam;
        private System.Windows.Forms.Label label_Wachtwoord;
        private System.Windows.Forms.TextBox textbox_Gebruikersnaam;
        private System.Windows.Forms.TextBox textbox_Wachtwoord;
        private System.Windows.Forms.Button button_Inloggen;
        private System.Windows.Forms.PictureBox ExitIcon;
        private System.Windows.Forms.PictureBox MinimizeIcon;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}

