﻿namespace Gimpies
{
    partial class Users_Admin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Users_Admin));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.MinimizeIcon = new System.Windows.Forms.PictureBox();
            this.ExitIcon = new System.Windows.Forms.PictureBox();
            this.button_Verwijderen2 = new System.Windows.Forms.Button();
            this.button_Verwijderen = new System.Windows.Forms.Button();
            this.GridUsers = new System.Windows.Forms.DataGridView();
            this.BackIcon = new System.Windows.Forms.PictureBox();
            this.label_wachtwoord = new System.Windows.Forms.Label();
            this.label_gebruikersnaam = new System.Windows.Forms.Label();
            this.textbox_Gebruikersnaam = new System.Windows.Forms.TextBox();
            this.button_Toevoegen = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.button_Users = new System.Windows.Forms.Button();
            this.button_Admins = new System.Windows.Forms.Button();
            this.GridAdmins = new System.Windows.Forms.DataGridView();
            this.label_Kop1 = new System.Windows.Forms.Label();
            this.textbox_Wachtwoord = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.BackIcon2 = new System.Windows.Forms.PictureBox();
            this.panel_wijzigen = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.textbox_wijzig2 = new System.Windows.Forms.TextBox();
            this.label_kop2 = new System.Windows.Forms.Label();
            this.label_wijzig2 = new System.Windows.Forms.Label();
            this.label_wijzig1 = new System.Windows.Forms.Label();
            this.textbox_wijzig1 = new System.Windows.Forms.TextBox();
            this.button_wijzigen2 = new System.Windows.Forms.Button();
            this.button_wijzigen = new System.Windows.Forms.Button();
            this.panel_verwijderen = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel_nieuw = new System.Windows.Forms.Panel();
            this.button_GebruikerToevoegen = new System.Windows.Forms.Button();
            this.button_GebuikerVerwijderen = new System.Windows.Forms.Button();
            this.button1_GegevensWijzigen = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MinimizeIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExitIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridUsers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BackIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridAdmins)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BackIcon2)).BeginInit();
            this.panel_wijzigen.SuspendLayout();
            this.panel_verwijderen.SuspendLayout();
            this.panel_nieuw.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.MinimizeIcon);
            this.panel1.Controls.Add(this.ExitIcon);
            this.panel1.Location = new System.Drawing.Point(-3, 1);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(827, 113);
            this.panel1.TabIndex = 10;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseUp);
            // 
            // MinimizeIcon
            // 
            this.MinimizeIcon.BackColor = System.Drawing.Color.Transparent;
            this.MinimizeIcon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MinimizeIcon.Image = ((System.Drawing.Image)(resources.GetObject("MinimizeIcon.Image")));
            this.MinimizeIcon.Location = new System.Drawing.Point(708, 36);
            this.MinimizeIcon.Margin = new System.Windows.Forms.Padding(4);
            this.MinimizeIcon.Name = "MinimizeIcon";
            this.MinimizeIcon.Size = new System.Drawing.Size(44, 44);
            this.MinimizeIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.MinimizeIcon.TabIndex = 10;
            this.MinimizeIcon.TabStop = false;
            this.MinimizeIcon.Click += new System.EventHandler(this.MinimizeIcon_Click);
            // 
            // ExitIcon
            // 
            this.ExitIcon.BackColor = System.Drawing.Color.Transparent;
            this.ExitIcon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ExitIcon.Image = ((System.Drawing.Image)(resources.GetObject("ExitIcon.Image")));
            this.ExitIcon.Location = new System.Drawing.Point(760, 32);
            this.ExitIcon.Margin = new System.Windows.Forms.Padding(4);
            this.ExitIcon.Name = "ExitIcon";
            this.ExitIcon.Size = new System.Drawing.Size(48, 48);
            this.ExitIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ExitIcon.TabIndex = 9;
            this.ExitIcon.TabStop = false;
            this.ExitIcon.Click += new System.EventHandler(this.ExitIcon_Click);
            // 
            // button_Verwijderen2
            // 
            this.button_Verwijderen2.BackColor = System.Drawing.Color.Gainsboro;
            this.button_Verwijderen2.FlatAppearance.BorderColor = System.Drawing.Color.Brown;
            this.button_Verwijderen2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red;
            this.button_Verwijderen2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Verwijderen2.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Verwijderen2.Location = new System.Drawing.Point(47, 203);
            this.button_Verwijderen2.Margin = new System.Windows.Forms.Padding(4);
            this.button_Verwijderen2.Name = "button_Verwijderen2";
            this.button_Verwijderen2.Size = new System.Drawing.Size(113, 32);
            this.button_Verwijderen2.TabIndex = 57;
            this.button_Verwijderen2.Text = "Verwijderen";
            this.button_Verwijderen2.UseVisualStyleBackColor = false;
            this.button_Verwijderen2.Click += new System.EventHandler(this.button_Verwijderen2_Click);
            // 
            // button_Verwijderen
            // 
            this.button_Verwijderen.BackColor = System.Drawing.Color.Gainsboro;
            this.button_Verwijderen.FlatAppearance.BorderColor = System.Drawing.Color.Brown;
            this.button_Verwijderen.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red;
            this.button_Verwijderen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Verwijderen.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Verwijderen.Location = new System.Drawing.Point(47, 203);
            this.button_Verwijderen.Margin = new System.Windows.Forms.Padding(4);
            this.button_Verwijderen.Name = "button_Verwijderen";
            this.button_Verwijderen.Size = new System.Drawing.Size(113, 32);
            this.button_Verwijderen.TabIndex = 56;
            this.button_Verwijderen.Text = "Verwijderen";
            this.button_Verwijderen.UseVisualStyleBackColor = false;
            this.button_Verwijderen.Click += new System.EventHandler(this.button_Verwijderen_Click);
            // 
            // GridUsers
            // 
            this.GridUsers.AllowUserToAddRows = false;
            this.GridUsers.AllowUserToDeleteRows = false;
            this.GridUsers.AllowUserToResizeColumns = false;
            this.GridUsers.AllowUserToResizeRows = false;
            this.GridUsers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GridUsers.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.GridUsers.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.GridUsers.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GridUsers.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.GridUsers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.GridUsers.DefaultCellStyle = dataGridViewCellStyle6;
            this.GridUsers.EnableHeadersVisualStyles = false;
            this.GridUsers.GridColor = System.Drawing.Color.Goldenrod;
            this.GridUsers.Location = new System.Drawing.Point(450, 210);
            this.GridUsers.Margin = new System.Windows.Forms.Padding(4);
            this.GridUsers.MultiSelect = false;
            this.GridUsers.Name = "GridUsers";
            this.GridUsers.ReadOnly = true;
            this.GridUsers.RowHeadersVisible = false;
            this.GridUsers.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.GridUsers.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.GridUsers.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridUsers.Size = new System.Drawing.Size(332, 293);
            this.GridUsers.TabIndex = 14;
            this.GridUsers.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridUsers_CellClick);
            // 
            // BackIcon
            // 
            this.BackIcon.BackColor = System.Drawing.Color.Transparent;
            this.BackIcon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BackIcon.Image = ((System.Drawing.Image)(resources.GetObject("BackIcon.Image")));
            this.BackIcon.Location = new System.Drawing.Point(28, 90);
            this.BackIcon.Margin = new System.Windows.Forms.Padding(4);
            this.BackIcon.Name = "BackIcon";
            this.BackIcon.Size = new System.Drawing.Size(47, 47);
            this.BackIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BackIcon.TabIndex = 19;
            this.BackIcon.TabStop = false;
            this.BackIcon.Click += new System.EventHandler(this.BackIcon_Click);
            // 
            // label_wachtwoord
            // 
            this.label_wachtwoord.AutoSize = true;
            this.label_wachtwoord.BackColor = System.Drawing.Color.Transparent;
            this.label_wachtwoord.Font = new System.Drawing.Font("Raleway", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_wachtwoord.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.label_wachtwoord.Location = new System.Drawing.Point(17, 148);
            this.label_wachtwoord.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_wachtwoord.Name = "label_wachtwoord";
            this.label_wachtwoord.Size = new System.Drawing.Size(169, 18);
            this.label_wachtwoord.TabIndex = 34;
            this.label_wachtwoord.Text = "Maak een wachtwoord:";
            // 
            // label_gebruikersnaam
            // 
            this.label_gebruikersnaam.AutoSize = true;
            this.label_gebruikersnaam.BackColor = System.Drawing.Color.Transparent;
            this.label_gebruikersnaam.Font = new System.Drawing.Font("Raleway", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_gebruikersnaam.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.label_gebruikersnaam.Location = new System.Drawing.Point(17, 88);
            this.label_gebruikersnaam.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_gebruikersnaam.Name = "label_gebruikersnaam";
            this.label_gebruikersnaam.Size = new System.Drawing.Size(195, 18);
            this.label_gebruikersnaam.TabIndex = 31;
            this.label_gebruikersnaam.Text = "Maak een gebruikersnaam:";
            // 
            // textbox_Gebruikersnaam
            // 
            this.textbox_Gebruikersnaam.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textbox_Gebruikersnaam.ForeColor = System.Drawing.Color.Black;
            this.textbox_Gebruikersnaam.Location = new System.Drawing.Point(20, 110);
            this.textbox_Gebruikersnaam.Margin = new System.Windows.Forms.Padding(4);
            this.textbox_Gebruikersnaam.Name = "textbox_Gebruikersnaam";
            this.textbox_Gebruikersnaam.Size = new System.Drawing.Size(365, 30);
            this.textbox_Gebruikersnaam.TabIndex = 30;
            // 
            // button_Toevoegen
            // 
            this.button_Toevoegen.BackColor = System.Drawing.Color.Gainsboro;
            this.button_Toevoegen.FlatAppearance.BorderColor = System.Drawing.Color.Aqua;
            this.button_Toevoegen.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button_Toevoegen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Toevoegen.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Toevoegen.Location = new System.Drawing.Point(253, 223);
            this.button_Toevoegen.Margin = new System.Windows.Forms.Padding(4);
            this.button_Toevoegen.Name = "button_Toevoegen";
            this.button_Toevoegen.Size = new System.Drawing.Size(133, 37);
            this.button_Toevoegen.TabIndex = 36;
            this.button_Toevoegen.Text = "Toevoegen";
            this.button_Toevoegen.UseVisualStyleBackColor = false;
            this.button_Toevoegen.Click += new System.EventHandler(this.button_Toevoegen_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.BackColor = System.Drawing.Color.LightGray;
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBox1.Font = new System.Drawing.Font("Lucida Sans Unicode", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.ForeColor = System.Drawing.Color.Black;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(269, 63);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(4);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(116, 26);
            this.comboBox1.TabIndex = 38;
            // 
            // button_Users
            // 
            this.button_Users.BackColor = System.Drawing.Color.Gainsboro;
            this.button_Users.FlatAppearance.BorderColor = System.Drawing.Color.Gold;
            this.button_Users.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button_Users.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Users.Font = new System.Drawing.Font("Lucida Sans Unicode", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Users.ForeColor = System.Drawing.Color.MidnightBlue;
            this.button_Users.Location = new System.Drawing.Point(450, 169);
            this.button_Users.Margin = new System.Windows.Forms.Padding(4);
            this.button_Users.Name = "button_Users";
            this.button_Users.Size = new System.Drawing.Size(166, 42);
            this.button_Users.TabIndex = 39;
            this.button_Users.Text = "Users";
            this.button_Users.UseVisualStyleBackColor = false;
            this.button_Users.Click += new System.EventHandler(this.button_Users_Click);
            // 
            // button_Admins
            // 
            this.button_Admins.BackColor = System.Drawing.Color.Gainsboro;
            this.button_Admins.FlatAppearance.BorderColor = System.Drawing.Color.Gold;
            this.button_Admins.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button_Admins.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Admins.Font = new System.Drawing.Font("Lucida Sans Unicode", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Admins.ForeColor = System.Drawing.Color.Red;
            this.button_Admins.Location = new System.Drawing.Point(617, 169);
            this.button_Admins.Margin = new System.Windows.Forms.Padding(4);
            this.button_Admins.Name = "button_Admins";
            this.button_Admins.Size = new System.Drawing.Size(166, 42);
            this.button_Admins.TabIndex = 40;
            this.button_Admins.Text = "Admins";
            this.button_Admins.UseVisualStyleBackColor = false;
            this.button_Admins.Click += new System.EventHandler(this.button_Admins_Click);
            // 
            // GridAdmins
            // 
            this.GridAdmins.AllowUserToAddRows = false;
            this.GridAdmins.AllowUserToDeleteRows = false;
            this.GridAdmins.AllowUserToResizeColumns = false;
            this.GridAdmins.AllowUserToResizeRows = false;
            this.GridAdmins.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GridAdmins.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.GridAdmins.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.GridAdmins.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GridAdmins.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.GridAdmins.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.GridAdmins.DefaultCellStyle = dataGridViewCellStyle8;
            this.GridAdmins.EnableHeadersVisualStyles = false;
            this.GridAdmins.GridColor = System.Drawing.Color.Goldenrod;
            this.GridAdmins.Location = new System.Drawing.Point(450, 210);
            this.GridAdmins.Margin = new System.Windows.Forms.Padding(4);
            this.GridAdmins.MultiSelect = false;
            this.GridAdmins.Name = "GridAdmins";
            this.GridAdmins.ReadOnly = true;
            this.GridAdmins.RowHeadersVisible = false;
            this.GridAdmins.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.GridAdmins.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.GridAdmins.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridAdmins.Size = new System.Drawing.Size(332, 293);
            this.GridAdmins.TabIndex = 14;
            this.GridAdmins.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridAdmins_CellClick);
            // 
            // label_Kop1
            // 
            this.label_Kop1.AutoSize = true;
            this.label_Kop1.BackColor = System.Drawing.Color.Transparent;
            this.label_Kop1.Font = new System.Drawing.Font("Raleway", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Kop1.Location = new System.Drawing.Point(13, 16);
            this.label_Kop1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_Kop1.Name = "label_Kop1";
            this.label_Kop1.Size = new System.Drawing.Size(266, 35);
            this.label_Kop1.TabIndex = 29;
            this.label_Kop1.Text = "Nieuwe Gebruiker";
            this.label_Kop1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textbox_Wachtwoord
            // 
            this.textbox_Wachtwoord.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textbox_Wachtwoord.ForeColor = System.Drawing.Color.Black;
            this.textbox_Wachtwoord.Location = new System.Drawing.Point(20, 169);
            this.textbox_Wachtwoord.Margin = new System.Windows.Forms.Padding(4);
            this.textbox_Wachtwoord.Name = "textbox_Wachtwoord";
            this.textbox_Wachtwoord.Size = new System.Drawing.Size(365, 30);
            this.textbox_Wachtwoord.TabIndex = 32;
            this.textbox_Wachtwoord.UseSystemPasswordChar = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.ForeColor = System.Drawing.Color.Green;
            this.label3.Location = new System.Drawing.Point(17, 209);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 17);
            this.label3.TabIndex = 37;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label4.Font = new System.Drawing.Font("Raleway", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(220, 68);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 20);
            this.label4.TabIndex = 42;
            this.label4.Text = "Kies:";
            // 
            // BackIcon2
            // 
            this.BackIcon2.BackColor = System.Drawing.Color.Transparent;
            this.BackIcon2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BackIcon2.Image = ((System.Drawing.Image)(resources.GetObject("BackIcon2.Image")));
            this.BackIcon2.Location = new System.Drawing.Point(49, 169);
            this.BackIcon2.Margin = new System.Windows.Forms.Padding(4);
            this.BackIcon2.Name = "BackIcon2";
            this.BackIcon2.Size = new System.Drawing.Size(31, 34);
            this.BackIcon2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BackIcon2.TabIndex = 47;
            this.BackIcon2.TabStop = false;
            this.BackIcon2.Click += new System.EventHandler(this.BackIcon2_Click);
            // 
            // panel_wijzigen
            // 
            this.panel_wijzigen.Controls.Add(this.label2);
            this.panel_wijzigen.Controls.Add(this.textbox_wijzig2);
            this.panel_wijzigen.Controls.Add(this.label_kop2);
            this.panel_wijzigen.Controls.Add(this.label_wijzig2);
            this.panel_wijzigen.Controls.Add(this.label_wijzig1);
            this.panel_wijzigen.Controls.Add(this.textbox_wijzig1);
            this.panel_wijzigen.Controls.Add(this.button_wijzigen2);
            this.panel_wijzigen.Controls.Add(this.button_wijzigen);
            this.panel_wijzigen.Location = new System.Drawing.Point(38, 210);
            this.panel_wijzigen.Margin = new System.Windows.Forms.Padding(4);
            this.panel_wijzigen.Name = "panel_wijzigen";
            this.panel_wijzigen.Size = new System.Drawing.Size(377, 293);
            this.panel_wijzigen.TabIndex = 48;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Green;
            this.label2.Location = new System.Drawing.Point(59, 204);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 17);
            this.label2.TabIndex = 56;
            // 
            // textbox_wijzig2
            // 
            this.textbox_wijzig2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textbox_wijzig2.ForeColor = System.Drawing.Color.Black;
            this.textbox_wijzig2.Location = new System.Drawing.Point(61, 167);
            this.textbox_wijzig2.Margin = new System.Windows.Forms.Padding(4);
            this.textbox_wijzig2.Name = "textbox_wijzig2";
            this.textbox_wijzig2.Size = new System.Drawing.Size(265, 30);
            this.textbox_wijzig2.TabIndex = 54;
            // 
            // label_kop2
            // 
            this.label_kop2.AutoSize = true;
            this.label_kop2.BackColor = System.Drawing.Color.Transparent;
            this.label_kop2.Font = new System.Drawing.Font("Raleway", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_kop2.Location = new System.Drawing.Point(52, 36);
            this.label_kop2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_kop2.Name = "label_kop2";
            this.label_kop2.Size = new System.Drawing.Size(135, 35);
            this.label_kop2.TabIndex = 48;
            this.label_kop2.Text = "Wijzigen";
            this.label_kop2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label_wijzig2
            // 
            this.label_wijzig2.AutoSize = true;
            this.label_wijzig2.BackColor = System.Drawing.Color.Transparent;
            this.label_wijzig2.Font = new System.Drawing.Font("Raleway", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_wijzig2.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.label_wijzig2.Location = new System.Drawing.Point(59, 146);
            this.label_wijzig2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_wijzig2.Name = "label_wijzig2";
            this.label_wijzig2.Size = new System.Drawing.Size(169, 18);
            this.label_wijzig2.TabIndex = 52;
            this.label_wijzig2.Text = "Wijzig het wachtwoord:";
            // 
            // label_wijzig1
            // 
            this.label_wijzig1.AutoSize = true;
            this.label_wijzig1.BackColor = System.Drawing.Color.Transparent;
            this.label_wijzig1.Font = new System.Drawing.Font("Raleway", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_wijzig1.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.label_wijzig1.Location = new System.Drawing.Point(57, 89);
            this.label_wijzig1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_wijzig1.Name = "label_wijzig1";
            this.label_wijzig1.Size = new System.Drawing.Size(190, 18);
            this.label_wijzig1.TabIndex = 50;
            this.label_wijzig1.Text = "Wijzig de gebruikersnaam:";
            // 
            // textbox_wijzig1
            // 
            this.textbox_wijzig1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textbox_wijzig1.ForeColor = System.Drawing.Color.Black;
            this.textbox_wijzig1.Location = new System.Drawing.Point(60, 110);
            this.textbox_wijzig1.Margin = new System.Windows.Forms.Padding(4);
            this.textbox_wijzig1.Name = "textbox_wijzig1";
            this.textbox_wijzig1.Size = new System.Drawing.Size(265, 30);
            this.textbox_wijzig1.TabIndex = 49;
            // 
            // button_wijzigen2
            // 
            this.button_wijzigen2.BackColor = System.Drawing.Color.Gainsboro;
            this.button_wijzigen2.FlatAppearance.BorderColor = System.Drawing.Color.Aqua;
            this.button_wijzigen2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Coral;
            this.button_wijzigen2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_wijzigen2.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_wijzigen2.Location = new System.Drawing.Point(221, 233);
            this.button_wijzigen2.Margin = new System.Windows.Forms.Padding(4);
            this.button_wijzigen2.Name = "button_wijzigen2";
            this.button_wijzigen2.Size = new System.Drawing.Size(105, 43);
            this.button_wijzigen2.TabIndex = 55;
            this.button_wijzigen2.Text = "Wijzigen";
            this.button_wijzigen2.UseVisualStyleBackColor = false;
            this.button_wijzigen2.Click += new System.EventHandler(this.button_wijzigen2_Click);
            // 
            // button_wijzigen
            // 
            this.button_wijzigen.BackColor = System.Drawing.Color.Gainsboro;
            this.button_wijzigen.FlatAppearance.BorderColor = System.Drawing.Color.Aqua;
            this.button_wijzigen.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Coral;
            this.button_wijzigen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_wijzigen.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_wijzigen.Location = new System.Drawing.Point(221, 233);
            this.button_wijzigen.Margin = new System.Windows.Forms.Padding(4);
            this.button_wijzigen.Name = "button_wijzigen";
            this.button_wijzigen.Size = new System.Drawing.Size(105, 43);
            this.button_wijzigen.TabIndex = 53;
            this.button_wijzigen.Text = "Wijzigen";
            this.button_wijzigen.UseVisualStyleBackColor = false;
            this.button_wijzigen.Click += new System.EventHandler(this.button_wijzigen_Click);
            // 
            // panel_verwijderen
            // 
            this.panel_verwijderen.Controls.Add(this.label1);
            this.panel_verwijderen.Controls.Add(this.button_Verwijderen);
            this.panel_verwijderen.Controls.Add(this.button_Verwijderen2);
            this.panel_verwijderen.Location = new System.Drawing.Point(157, 210);
            this.panel_verwijderen.Margin = new System.Windows.Forms.Padding(4);
            this.panel_verwijderen.Name = "panel_verwijderen";
            this.panel_verwijderen.Size = new System.Drawing.Size(196, 286);
            this.panel_verwijderen.TabIndex = 58;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Raleway", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Brown;
            this.label1.Location = new System.Drawing.Point(42, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 123);
            this.label1.TabIndex = 58;
            this.label1.Text = "Selecteer de rij en klik hieronder op verwijderen";
            // 
            // panel_nieuw
            // 
            this.panel_nieuw.Controls.Add(this.label4);
            this.panel_nieuw.Controls.Add(this.comboBox1);
            this.panel_nieuw.Controls.Add(this.label3);
            this.panel_nieuw.Controls.Add(this.button_Toevoegen);
            this.panel_nieuw.Controls.Add(this.label_wachtwoord);
            this.panel_nieuw.Controls.Add(this.textbox_Gebruikersnaam);
            this.panel_nieuw.Controls.Add(this.label_gebruikersnaam);
            this.panel_nieuw.Controls.Add(this.textbox_Wachtwoord);
            this.panel_nieuw.Controls.Add(this.label_Kop1);
            this.panel_nieuw.Location = new System.Drawing.Point(38, 214);
            this.panel_nieuw.Margin = new System.Windows.Forms.Padding(4);
            this.panel_nieuw.Name = "panel_nieuw";
            this.panel_nieuw.Size = new System.Drawing.Size(404, 289);
            this.panel_nieuw.TabIndex = 49;
            // 
            // button_GebruikerToevoegen
            // 
            this.button_GebruikerToevoegen.BackColor = System.Drawing.Color.LightGray;
            this.button_GebruikerToevoegen.FlatAppearance.BorderColor = System.Drawing.Color.LimeGreen;
            this.button_GebruikerToevoegen.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LawnGreen;
            this.button_GebruikerToevoegen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_GebruikerToevoegen.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_GebruikerToevoegen.Location = new System.Drawing.Point(157, 250);
            this.button_GebruikerToevoegen.Margin = new System.Windows.Forms.Padding(4);
            this.button_GebruikerToevoegen.Name = "button_GebruikerToevoegen";
            this.button_GebruikerToevoegen.Size = new System.Drawing.Size(196, 44);
            this.button_GebruikerToevoegen.TabIndex = 50;
            this.button_GebruikerToevoegen.Text = "Gebruiker Toevoegen";
            this.button_GebruikerToevoegen.UseVisualStyleBackColor = false;
            this.button_GebruikerToevoegen.Click += new System.EventHandler(this.button_GebruikerToevoegen_Click);
            // 
            // button_GebuikerVerwijderen
            // 
            this.button_GebuikerVerwijderen.BackColor = System.Drawing.Color.LightGray;
            this.button_GebuikerVerwijderen.FlatAppearance.BorderColor = System.Drawing.Color.LightCoral;
            this.button_GebuikerVerwijderen.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Tomato;
            this.button_GebuikerVerwijderen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_GebuikerVerwijderen.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_GebuikerVerwijderen.Location = new System.Drawing.Point(157, 306);
            this.button_GebuikerVerwijderen.Margin = new System.Windows.Forms.Padding(4);
            this.button_GebuikerVerwijderen.Name = "button_GebuikerVerwijderen";
            this.button_GebuikerVerwijderen.Size = new System.Drawing.Size(196, 44);
            this.button_GebuikerVerwijderen.TabIndex = 51;
            this.button_GebuikerVerwijderen.Text = "Gebuiker verwijderen";
            this.button_GebuikerVerwijderen.UseVisualStyleBackColor = false;
            this.button_GebuikerVerwijderen.Click += new System.EventHandler(this.button_GebuikerVerwijderen_Click);
            // 
            // button1_GegevensWijzigen
            // 
            this.button1_GegevensWijzigen.BackColor = System.Drawing.Color.LightGray;
            this.button1_GegevensWijzigen.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button1_GegevensWijzigen.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SandyBrown;
            this.button1_GegevensWijzigen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1_GegevensWijzigen.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1_GegevensWijzigen.Location = new System.Drawing.Point(157, 362);
            this.button1_GegevensWijzigen.Margin = new System.Windows.Forms.Padding(4);
            this.button1_GegevensWijzigen.Name = "button1_GegevensWijzigen";
            this.button1_GegevensWijzigen.Size = new System.Drawing.Size(196, 44);
            this.button1_GegevensWijzigen.TabIndex = 60;
            this.button1_GegevensWijzigen.Text = "Gegevens wijzigen";
            this.button1_GegevensWijzigen.UseVisualStyleBackColor = false;
            this.button1_GegevensWijzigen.Click += new System.EventHandler(this.button1_GegevensWijzigen_Click);
            // 
            // Users_Admin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(824, 642);
            this.Controls.Add(this.panel_verwijderen);
            this.Controls.Add(this.BackIcon2);
            this.Controls.Add(this.button_Admins);
            this.Controls.Add(this.button_Users);
            this.Controls.Add(this.BackIcon);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button1_GegevensWijzigen);
            this.Controls.Add(this.button_GebuikerVerwijderen);
            this.Controls.Add(this.button_GebruikerToevoegen);
            this.Controls.Add(this.GridUsers);
            this.Controls.Add(this.GridAdmins);
            this.Controls.Add(this.panel_nieuw);
            this.Controls.Add(this.panel_wijzigen);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Users_Admin";
            this.Text = "Users";
            this.Load += new System.EventHandler(this.Users_Admin_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MinimizeIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExitIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridUsers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BackIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridAdmins)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BackIcon2)).EndInit();
            this.panel_wijzigen.ResumeLayout(false);
            this.panel_wijzigen.PerformLayout();
            this.panel_verwijderen.ResumeLayout(false);
            this.panel_nieuw.ResumeLayout(false);
            this.panel_nieuw.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox MinimizeIcon;
        private System.Windows.Forms.PictureBox ExitIcon;
        private System.Windows.Forms.PictureBox BackIcon;
        public System.Windows.Forms.DataGridView GridUsers;
        private System.Windows.Forms.Label label_wachtwoord;
        private System.Windows.Forms.Label label_gebruikersnaam;
        private System.Windows.Forms.TextBox textbox_Gebruikersnaam;
        private System.Windows.Forms.Button button_Toevoegen;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button button_Users;
        private System.Windows.Forms.Button button_Admins;
        private System.Windows.Forms.Label label_Kop1;
        private System.Windows.Forms.TextBox textbox_Wachtwoord;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox BackIcon2;
        private System.Windows.Forms.Panel panel_wijzigen;
        private System.Windows.Forms.Label label_kop2;
        private System.Windows.Forms.Button button_wijzigen;
        private System.Windows.Forms.Label label_wijzig2;
        private System.Windows.Forms.Label label_wijzig1;
        private System.Windows.Forms.TextBox textbox_wijzig1;
        private System.Windows.Forms.TextBox textbox_wijzig2;
        private System.Windows.Forms.Button button_wijzigen2;
        public System.Windows.Forms.DataGridView GridAdmins;
        private System.Windows.Forms.Button button_Verwijderen;
        private System.Windows.Forms.Button button_Verwijderen2;
        private System.Windows.Forms.Panel panel_nieuw;
        private System.Windows.Forms.Button button_GebruikerToevoegen;
        private System.Windows.Forms.Button button_GebuikerVerwijderen;
        private System.Windows.Forms.Panel panel_verwijderen;
        private System.Windows.Forms.Button button1_GegevensWijzigen;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}