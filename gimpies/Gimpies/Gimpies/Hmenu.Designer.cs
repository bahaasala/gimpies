﻿namespace Gimpies
{
    partial class Hmenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Hmenu));
            this.MinimizeIcon = new System.Windows.Forms.PictureBox();
            this.ExitIcon = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.LogoutIcon = new System.Windows.Forms.PictureBox();
            this.button_Bekijken = new System.Windows.Forms.Button();
            this.button_SchoenenToevoegen = new System.Windows.Forms.Button();
            this.PlusIcon = new System.Windows.Forms.PictureBox();
            this.CartIcon = new System.Windows.Forms.PictureBox();
            this.button_SchoenenVerwijderen = new System.Windows.Forms.Button();
            this.MinusIcon = new System.Windows.Forms.PictureBox();
            this.button_NieuwProduct = new System.Windows.Forms.Button();
            this.NewIcon = new System.Windows.Forms.PictureBox();
            this.UsersIcon = new System.Windows.Forms.PictureBox();
            this.button_DeGebruikers = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.MinimizeIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExitIcon)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LogoutIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlusIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CartIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinusIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NewIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsersIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // MinimizeIcon
            // 
            this.MinimizeIcon.BackColor = System.Drawing.Color.Transparent;
            this.MinimizeIcon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MinimizeIcon.Image = ((System.Drawing.Image)(resources.GetObject("MinimizeIcon.Image")));
            this.MinimizeIcon.Location = new System.Drawing.Point(330, 17);
            this.MinimizeIcon.Name = "MinimizeIcon";
            this.MinimizeIcon.Size = new System.Drawing.Size(33, 36);
            this.MinimizeIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.MinimizeIcon.TabIndex = 8;
            this.MinimizeIcon.TabStop = false;
            this.MinimizeIcon.Click += new System.EventHandler(this.MinimizeIcon_Click);
            // 
            // ExitIcon
            // 
            this.ExitIcon.BackColor = System.Drawing.Color.Transparent;
            this.ExitIcon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ExitIcon.Image = ((System.Drawing.Image)(resources.GetObject("ExitIcon.Image")));
            this.ExitIcon.Location = new System.Drawing.Point(369, 14);
            this.ExitIcon.Name = "ExitIcon";
            this.ExitIcon.Size = new System.Drawing.Size(36, 39);
            this.ExitIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ExitIcon.TabIndex = 7;
            this.ExitIcon.TabStop = false;
            this.ExitIcon.Click += new System.EventHandler(this.ExitIcon_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.LogoutIcon);
            this.panel1.Controls.Add(this.ExitIcon);
            this.panel1.Controls.Add(this.MinimizeIcon);
            this.panel1.Location = new System.Drawing.Point(-1, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(415, 76);
            this.panel1.TabIndex = 9;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseUp);
            // 
            // LogoutIcon
            // 
            this.LogoutIcon.BackColor = System.Drawing.Color.Transparent;
            this.LogoutIcon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.LogoutIcon.Image = ((System.Drawing.Image)(resources.GetObject("LogoutIcon.Image")));
            this.LogoutIcon.Location = new System.Drawing.Point(16, 17);
            this.LogoutIcon.Name = "LogoutIcon";
            this.LogoutIcon.Size = new System.Drawing.Size(35, 38);
            this.LogoutIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.LogoutIcon.TabIndex = 0;
            this.LogoutIcon.TabStop = false;
            this.LogoutIcon.Click += new System.EventHandler(this.LogoutIcon_Click);
            // 
            // button_Bekijken
            // 
            this.button_Bekijken.BackColor = System.Drawing.Color.LightGray;
            this.button_Bekijken.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.button_Bekijken.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.button_Bekijken.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Bekijken.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Bekijken.ForeColor = System.Drawing.Color.Black;
            this.button_Bekijken.Location = new System.Drawing.Point(118, 141);
            this.button_Bekijken.Name = "button_Bekijken";
            this.button_Bekijken.Size = new System.Drawing.Size(184, 43);
            this.button_Bekijken.TabIndex = 10;
            this.button_Bekijken.Text = "Schoenen Bekijken";
            this.button_Bekijken.UseVisualStyleBackColor = false;
            this.button_Bekijken.Click += new System.EventHandler(this.BekijkenButton_Click);
            // 
            // button_SchoenenToevoegen
            // 
            this.button_SchoenenToevoegen.BackColor = System.Drawing.Color.LightGray;
            this.button_SchoenenToevoegen.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.button_SchoenenToevoegen.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.button_SchoenenToevoegen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_SchoenenToevoegen.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_SchoenenToevoegen.ForeColor = System.Drawing.Color.Black;
            this.button_SchoenenToevoegen.Location = new System.Drawing.Point(118, 242);
            this.button_SchoenenToevoegen.Name = "button_SchoenenToevoegen";
            this.button_SchoenenToevoegen.Size = new System.Drawing.Size(184, 43);
            this.button_SchoenenToevoegen.TabIndex = 18;
            this.button_SchoenenToevoegen.Text = "Schoenen Toevoegen";
            this.button_SchoenenToevoegen.UseVisualStyleBackColor = false;
            this.button_SchoenenToevoegen.Click += new System.EventHandler(this.ToevoegenButton_Click);
            // 
            // PlusIcon
            // 
            this.PlusIcon.BackColor = System.Drawing.Color.Transparent;
            this.PlusIcon.Image = ((System.Drawing.Image)(resources.GetObject("PlusIcon.Image")));
            this.PlusIcon.Location = new System.Drawing.Point(83, 247);
            this.PlusIcon.Name = "PlusIcon";
            this.PlusIcon.Size = new System.Drawing.Size(29, 30);
            this.PlusIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PlusIcon.TabIndex = 15;
            this.PlusIcon.TabStop = false;
            // 
            // CartIcon
            // 
            this.CartIcon.BackColor = System.Drawing.Color.Transparent;
            this.CartIcon.Image = ((System.Drawing.Image)(resources.GetObject("CartIcon.Image")));
            this.CartIcon.Location = new System.Drawing.Point(79, 143);
            this.CartIcon.Name = "CartIcon";
            this.CartIcon.Size = new System.Drawing.Size(36, 37);
            this.CartIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.CartIcon.TabIndex = 14;
            this.CartIcon.TabStop = false;
            // 
            // button_SchoenenVerwijderen
            // 
            this.button_SchoenenVerwijderen.BackColor = System.Drawing.Color.LightGray;
            this.button_SchoenenVerwijderen.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.button_SchoenenVerwijderen.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.button_SchoenenVerwijderen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_SchoenenVerwijderen.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_SchoenenVerwijderen.ForeColor = System.Drawing.Color.Black;
            this.button_SchoenenVerwijderen.Location = new System.Drawing.Point(118, 293);
            this.button_SchoenenVerwijderen.Name = "button_SchoenenVerwijderen";
            this.button_SchoenenVerwijderen.Size = new System.Drawing.Size(184, 43);
            this.button_SchoenenVerwijderen.TabIndex = 18;
            this.button_SchoenenVerwijderen.Text = "Schoenen Verwijderen";
            this.button_SchoenenVerwijderen.UseVisualStyleBackColor = false;
            this.button_SchoenenVerwijderen.Click += new System.EventHandler(this.VerwijderenButton_Click);
            // 
            // MinusIcon
            // 
            this.MinusIcon.BackColor = System.Drawing.Color.Transparent;
            this.MinusIcon.Image = ((System.Drawing.Image)(resources.GetObject("MinusIcon.Image")));
            this.MinusIcon.Location = new System.Drawing.Point(83, 299);
            this.MinusIcon.Name = "MinusIcon";
            this.MinusIcon.Size = new System.Drawing.Size(29, 30);
            this.MinusIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.MinusIcon.TabIndex = 19;
            this.MinusIcon.TabStop = false;
            // 
            // button_NieuwProduct
            // 
            this.button_NieuwProduct.BackColor = System.Drawing.Color.LightGray;
            this.button_NieuwProduct.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.button_NieuwProduct.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.button_NieuwProduct.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_NieuwProduct.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_NieuwProduct.ForeColor = System.Drawing.Color.Black;
            this.button_NieuwProduct.Location = new System.Drawing.Point(118, 191);
            this.button_NieuwProduct.Name = "button_NieuwProduct";
            this.button_NieuwProduct.Size = new System.Drawing.Size(184, 43);
            this.button_NieuwProduct.TabIndex = 20;
            this.button_NieuwProduct.Text = "Nieuw Product";
            this.button_NieuwProduct.UseVisualStyleBackColor = false;
            this.button_NieuwProduct.Click += new System.EventHandler(this.NieuwProduct_Click);
            // 
            // NewIcon
            // 
            this.NewIcon.BackColor = System.Drawing.Color.Transparent;
            this.NewIcon.Image = ((System.Drawing.Image)(resources.GetObject("NewIcon.Image")));
            this.NewIcon.Location = new System.Drawing.Point(83, 196);
            this.NewIcon.Name = "NewIcon";
            this.NewIcon.Size = new System.Drawing.Size(31, 32);
            this.NewIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.NewIcon.TabIndex = 21;
            this.NewIcon.TabStop = false;
            // 
            // UsersIcon
            // 
            this.UsersIcon.BackColor = System.Drawing.Color.Transparent;
            this.UsersIcon.Image = ((System.Drawing.Image)(resources.GetObject("UsersIcon.Image")));
            this.UsersIcon.Location = new System.Drawing.Point(16, 83);
            this.UsersIcon.Name = "UsersIcon";
            this.UsersIcon.Size = new System.Drawing.Size(29, 30);
            this.UsersIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.UsersIcon.TabIndex = 25;
            this.UsersIcon.TabStop = false;
            // 
            // button_DeGebruikers
            // 
            this.button_DeGebruikers.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.button_DeGebruikers.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.button_DeGebruikers.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.button_DeGebruikers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_DeGebruikers.Font = new System.Drawing.Font("Verdana", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_DeGebruikers.ForeColor = System.Drawing.Color.Black;
            this.button_DeGebruikers.Location = new System.Drawing.Point(47, 82);
            this.button_DeGebruikers.Name = "button_DeGebruikers";
            this.button_DeGebruikers.Size = new System.Drawing.Size(114, 30);
            this.button_DeGebruikers.TabIndex = 24;
            this.button_DeGebruikers.Text = "De Gebruikers";
            this.button_DeGebruikers.UseVisualStyleBackColor = false;
            this.button_DeGebruikers.Click += new System.EventHandler(this.button_DeGebruikers_Click);
            // 
            // Hmenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(413, 422);
            this.Controls.Add(this.UsersIcon);
            this.Controls.Add(this.button_DeGebruikers);
            this.Controls.Add(this.NewIcon);
            this.Controls.Add(this.button_NieuwProduct);
            this.Controls.Add(this.MinusIcon);
            this.Controls.Add(this.button_SchoenenVerwijderen);
            this.Controls.Add(this.PlusIcon);
            this.Controls.Add(this.CartIcon);
            this.Controls.Add(this.button_Bekijken);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button_SchoenenToevoegen);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Hmenu";
            this.Text = "Form2";
            ((System.ComponentModel.ISupportInitialize)(this.MinimizeIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExitIcon)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LogoutIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlusIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CartIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinusIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NewIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsersIcon)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox MinimizeIcon;
        private System.Windows.Forms.PictureBox ExitIcon;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox LogoutIcon;
        private System.Windows.Forms.Button button_Bekijken;
        private System.Windows.Forms.Button button_SchoenenToevoegen;
        private System.Windows.Forms.PictureBox PlusIcon;
        private System.Windows.Forms.PictureBox CartIcon;
        private System.Windows.Forms.Button button_SchoenenVerwijderen;
        private System.Windows.Forms.PictureBox MinusIcon;
        private System.Windows.Forms.Button button_NieuwProduct;
        private System.Windows.Forms.PictureBox NewIcon;
        private System.Windows.Forms.PictureBox UsersIcon;
        private System.Windows.Forms.Button button_DeGebruikers;
    }
}